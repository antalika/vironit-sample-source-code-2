﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UnityFramework.UI
{
    /// <summary>
    /// Horizontal alignment of element.
    /// </summary>
    public enum HorizontalAlignment
    {
        Center = 0,
        Left,
        Right,
        // Stretch  # TODO
    }

    /// <summary>
    /// Vertical alignment of element.
    /// </summary>
    public enum VerticalAlignment
    {
        Center = 0,
        Top,
        Bottom,
        // Stretch  # TODO
    }

    /// <summary>
    /// Virtualized ListView.
    /// </summary>
    public class ListView : ScrollRect
    {
        [SerializeField]
        private RectTransformDimensions _dimensions = null;

        [SerializeField]
        private Vector2 _itemSize = new Vector2(100, 100);
        [SerializeField]
        private Vector2 _space = Vector2.zero;

        [SerializeField]
        private HorizontalAlignment _horizontalAlignment = HorizontalAlignment.Center;
        [SerializeField]
        private VerticalAlignment _verticalAlignment = VerticalAlignment.Center;

#pragma warning disable 0649
        [SerializeField]
        private Template _itemTemplate;
        [SerializeField]
        private TemplateSelector _itemTemplateSelector;
#pragma warning restore 0649

        [SerializeField]
        private RectOffset _padding = new RectOffset();

        private Vector2 _currentViewSize = Vector2.zero;
        private Range _currentViewRange = new Range(0, 0);
        private GridSize _currentGridSize = new GridSize(0, 0);

        Dictionary<int, IListViewItem> _itemsView = new Dictionary<int, IListViewItem>();

        private IEnumerable _itemsSource = null;

        /// <summary>
        /// A collection that is used to generate the content of the ItemsControl. The default is null.
        /// </summary>
        public IEnumerable ItemsSource
        {
            get { return _itemsSource; }
            set
            {
                if (value != _itemsSource)
                {
                    var oldValue = _itemsSource;
                    _itemsSource = value;
                    OnItemsSourceChanged(oldValue, _itemsSource);
                }
            }
        }

        protected virtual void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            ResetVisibleItems();
            RefreshVisibleItems();
        }


        /// <summary>
        /// Selects the template for content.
        /// </summary>
        /// <returns>The template of view.</returns>
        /// <param name="content">Content to display.</param>
        private Template SelectTemplate(object content)
        {
            if (_itemTemplate != null)
                return _itemTemplate;

            if (_itemTemplateSelector != null)
                return _itemTemplateSelector.Select(content);

            return null;
        }



        /// <summary>
        /// Calculates the size of the grid by size of the view and count of items.
        /// </summary>
        /// <returns>The grid size.</returns>
        /// <param name="viewSize">View size.</param>
        /// <param name="count">Items count.</param>
        private GridSize CalcGridSize(Vector2 viewSize, int count)
        {
            int columnCount = 0, rowCount = 0;

            if (vertical)
            {
                columnCount = 1;
                if (_itemSize.x > 0)
                    columnCount = Mathf.FloorToInt((viewSize.x + _space.x) / (_itemSize.x + _space.x));

                rowCount = Mathf.CeilToInt((float)count / columnCount);
            }
            else if (horizontal)
            {
                rowCount = 1;
                if (_itemSize.y > 0)
                    rowCount = Mathf.FloorToInt((viewSize.y + _space.y) / (_itemSize.y + _space.y));

                columnCount = Mathf.CeilToInt((float)count / rowCount);
            }

            return new GridSize(columnCount, rowCount);
        }

        /// <summary>
        /// Gets the size of the viewport.
        /// </summary>
        /// <returns>The viewport size.</returns>
        private Vector2 GetViewportSize()
        {
            var rect = viewport.rect; //((RectTransform)this.transform).rect;

            var width = rect.width - _padding.horizontal;
            var height = rect.height - _padding.vertical;

            return new Vector2(width, height);
        }

        /// <summary>
        /// Gets the size of the content.
        /// </summary>
        /// <returns>The content size.</returns>
        /// <param name="gridSize">Grid size.</param>
        private Vector2 GetContentSize(GridSize gridSize)
        {
            var width = (_itemSize.x + _space.x) * gridSize.columnCount - _space.x + _padding.horizontal;
            var height = (_itemSize.y + _space.y) * gridSize.rowCount - _space.y + _padding.vertical;

            return new Vector2(width, height);
        }

        /// <summary>
        /// Gets the item position in the list by its index.
        /// </summary>
        /// <returns>The item position.</returns>
        /// <param name="gridSize">Grid size.</param>
        /// <param name="viewSize">View size.</param>
        /// <param name="index">Item index.</param>
        private Vector3 GetItemPosition(GridSize gridSize, Vector2 viewSize, int index)
        {
            float offsetX = 0, offsetY = 0;

            var columnCount = gridSize.columnCount;
            if (columnCount <= 0) columnCount = 1;
            var rowCount = gridSize.rowCount;
            if (rowCount <= 0) rowCount = 1;

            int row = 0, column = 0;
            if (vertical)
            {
                row = Mathf.FloorToInt(index / columnCount);
                column = index % columnCount;

                if (_itemSize.x > 0)
                {
                    offsetX = viewSize.x - ((_itemSize.x + _space.x) * columnCount - _space.x);
                    switch (_horizontalAlignment)
                    {
                        case HorizontalAlignment.Center:
                            offsetX = offsetX * 0.5f;
                            break;
                        case HorizontalAlignment.Left:
                            offsetX = 0;
                            break;
                        case HorizontalAlignment.Right:
                            // NOTE: Do nothing
                            break;
                        default:
                            throw new NotSupportedException();
                    }
                }
            }
            else if (horizontal)
            {
                row = index % rowCount;
                column = Mathf.FloorToInt(index / rowCount);

                if (_itemSize.y > 0)
                {
                    offsetY = -(viewSize.y - ((_itemSize.y + _space.y) * rowCount - _space.y));
                    switch (_verticalAlignment)
                    {
                        case VerticalAlignment.Center:
                            offsetY = offsetY * 0.5f;
                            break;
                        case VerticalAlignment.Top:
                            offsetY = 0;
                            break;
                        case VerticalAlignment.Bottom:
                            // NOTE: Do nothing
                            break;
                        default:
                            throw new NotSupportedException();
                    }
                }
            }

            var x = offsetX + column * (_itemSize.x + _space.x) + _padding.left;
            var y = offsetY - (row * (_itemSize.y + _space.y) + _padding.top);

            return new Vector3(x, y, 0);
        }


        /// <summary>
        /// Refreshes visible items of the list.
        /// </summary>
        /// <returns><c>true</c>, if range was displayed, <c>false</c> otherwise.</returns>
        /// <param name="range">Range to display.</param>
        private bool DisplayRange(Range range)
        {
            int itemsCount = 0;

            var itemsSource = ItemsSource;
            if (itemsSource != null)
                itemsCount = itemsSource.Count();

            var viewSize = GetViewportSize();

            var gridSize = CalcGridSize(viewSize, itemsCount);


            var min = range.start;
            var max = min + range.count - 1;

            var currentIndices = _itemsView.Keys.ToList();
            for (int i = 0; i < currentIndices.Count; i++)
            {
                var item = currentIndices[i];
                if (item < min || item > max)
                {
                    var view = _itemsView[item];
                    _itemsView.Remove(item);

                    view.Content = null;
                    view.Recycle();

                    currentIndices.RemoveAt(i);
                    i--;
                }
            }

            for (int i = min; i <= max; i++)
            {
                IListViewItem view = null;
                MonoBehaviour viewBehaviour = null;
                if (_itemsView.TryGetValue(i, out view))
                {
                    viewBehaviour = (MonoBehaviour)view;
                }
                else
                {
                    var item = itemsSource.ElementAt(i);
                    var itemTemplate = SelectTemplate(item);
                    if (itemTemplate == null)
                        continue;

                    view = (IListViewItem)itemTemplate.CreateView();
                    view.Template = itemTemplate;
                    view.Content = item;

                    viewBehaviour = (MonoBehaviour)view;
                    viewBehaviour.name = string.Format("[{0}] #{1:000}", view.GetType().Name, i);
                    viewBehaviour.transform.SetParent(content, false);

                    _itemsView.Add(i, view);
                }

                var t = (RectTransform)viewBehaviour.transform;
                var pos = t.anchoredPosition;

                var targetPos = GetItemPosition(gridSize, viewSize, i);
                pos = pos.SetX(targetPos.x).SetY(targetPos.y);

                t.anchoredPosition = pos;
            }

            return true;
        }


        /// <summary>
        /// Updates the visible items.
        /// </summary>
        public void UpdateItems()
        {
            RefreshVisibleItems();
        }

        /// <summary>
        /// Resets the visible items.
        /// </summary>
        private void ResetVisibleItems()
        {
            if (IsDestroyed())
                return;
            

            _currentViewRange = new Range(0, 0);
            DisplayRange(_currentViewRange);
        }

        /// <summary>
        /// Refreshes the visible items if needed.
        /// </summary>
        private void RefreshVisibleItems()
        {
            if (IsDestroyed())
                return;
            

            int itemsCount = 0;
            var itemsSource = ItemsSource;
            if (itemsSource != null)
                itemsCount = itemsSource.Count();


            var viewSize = GetViewportSize();

            var gridSize = CalcGridSize(viewSize, itemsCount);
            var contentSize = GetContentSize(gridSize);

            var size = content.sizeDelta;
            if (vertical)
                size = size.SetY(contentSize.y);
            else if (horizontal)
                size = size.SetX(contentSize.x);

            content.sizeDelta = size;

            var range = GetVisibleRange();
            if (_currentViewRange != range ||
                _currentGridSize != gridSize ||
                _currentViewSize != viewSize)
            {
                _currentViewRange = range;
                _currentGridSize = gridSize;
                _currentViewSize = viewSize;

                DisplayRange(_currentViewRange);
            }
        }

        /// <summary>
        /// Initialization of the instance.
        /// </summary>
        protected override void Awake()
        {
            base.Awake();

            if (_dimensions == null)
            {
                _dimensions = this.viewport.gameObject.GetComponent<RectTransformDimensions>();
                if (_dimensions == null)
                    _dimensions = this.viewport.gameObject.AddComponent<RectTransformDimensions>();
            }

            if (_dimensions != null)
                _dimensions.onChanged.AddListener(OnRectTransformDimensionsChanged);

            this.onValueChanged.AddListener(OnScrollRectValueChanged);
        }

        /// <summary>
        /// Deinitialization of the instance.
        /// </summary>
        protected override void OnDestroy()
        {
            if (_dimensions != null)
                _dimensions.onChanged.RemoveListener(OnRectTransformDimensionsChanged);

            this.onValueChanged.RemoveListener(OnScrollRectValueChanged);

            base.OnDestroy();
        }


        /// <summary>
        /// Gets the visible range of the list.
        /// </summary>
        /// <returns>The visible range.</returns>
        private Range GetVisibleRange()
        {
            int start = 0;
            int count = 0;

            var itemsSource = ItemsSource;
            if (itemsSource != null)
            {
                var itemsCount = itemsSource.Count();

                var rect = viewport.rect;

                var normPos = new Vector2(horizontalNormalizedPosition, 1 - verticalNormalizedPosition);
                normPos.x = Mathf.Clamp(normPos.x, 0, 1);
                normPos.y = Mathf.Clamp(normPos.y, 0, 1);

                var itemSize = _itemSize;
                var viewSize = GetViewportSize();

                var gridSize = CalcGridSize(viewSize, itemsCount);
                var contentSize = GetContentSize(gridSize);

                var minPos = Vector2.zero;
                var maxPos = Vector2.zero;

                viewSize = rect.size;


                minPos.x = (contentSize.x - viewSize.x) * normPos.x - _padding.left;
                maxPos.x = minPos.x + viewSize.x;

                minPos.y = (contentSize.y - viewSize.y) * normPos.y - _padding.top;
                maxPos.y = minPos.y + viewSize.y;


                int minIndex = 0, maxIndex = 0;

                if (vertical)
                {
                    minIndex = Mathf.FloorToInt(minPos.y / (itemSize.y + _space.y)) * gridSize.columnCount;
                    maxIndex = (Mathf.FloorToInt(maxPos.y / (itemSize.y + _space.y)) + 1) * gridSize.columnCount - 1;
                }
                else if (horizontal)
                {
                    minIndex = Mathf.FloorToInt(minPos.x / (itemSize.x + _space.x)) * gridSize.rowCount;
                    maxIndex = (Mathf.FloorToInt(maxPos.x / (itemSize.x + _space.x)) + 1) * gridSize.rowCount - 1;
                }

                minIndex = Mathf.Clamp(minIndex, 0, itemsCount - 1);
                maxIndex = Mathf.Clamp(maxIndex, 0, itemsCount - 1);

                start = Mathf.Clamp(minIndex, 0, itemsCount - 1);
                count = Mathf.Clamp(maxIndex - minIndex + 1, 0, itemsCount);
            }

            return new Range(start, count);
        }


        /// <summary>
        /// Refreshes visible items on scroll position changed.
        /// </summary>
        /// <param name="value">Scroll position.</param>
        private void OnScrollRectValueChanged(Vector2 value)
        {
            UpdateItems();
        }

        /// <summary>
        /// Refreshes visible items on size of view changed.
        /// </summary>
        private void OnRectTransformDimensionsChanged()
        {
            UpdateItems();
        }


        #region Events forwarding
        private bool ValidateScroll(Vector2 delta)
        {
            if (horizontal && !vertical)
            {
                if (Mathf.Abs(delta.y) > Mathf.Abs(delta.x))
                    return false;
            }
            else if (vertical && !horizontal)
            {
                if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y))
                    return false;
            }

            return true;
        }

        private bool _forwardEvents = false;
        private ScrollRect _eventsTarget = null;

        public override void OnBeginDrag(PointerEventData eventData)
        {
            if (!ValidateScroll(eventData.delta))
            {
                var t = transform.parent;
                if (t != null)
                {
                    _eventsTarget = t.FindInParents<ScrollRect>();
                    if (_eventsTarget != null)
                    {
                        _forwardEvents = true;
                        _eventsTarget.OnBeginDrag(eventData);
                        return;
                    }
                }
            }

            base.OnBeginDrag(eventData);
        }

        public override void OnDrag(PointerEventData eventData)
        {
            if (_forwardEvents)
            {
                _eventsTarget.OnDrag(eventData);
                return;
            }

            base.OnDrag(eventData);
        }

        public override void OnEndDrag(PointerEventData eventData)
        {
            if (_forwardEvents)
            {
                _forwardEvents = false;

                _eventsTarget.OnEndDrag(eventData);
                _eventsTarget = null;

                return;
            }

            base.OnEndDrag(eventData);
        }
        #endregion
    }
}
